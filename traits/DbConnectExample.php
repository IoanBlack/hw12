<?php

trait DbConnect
{
    protected static $db = NULL;
    protected static $dbHost = '';
    protected static $dbUser = '';
    protected static $dbPassword = '';
    protected static $dbName = '';

    protected static function setDb() {
        static::$db = new PDO("mysql:host=" . static::$dbHost . ";dbname=" . static::$dbName,static::$dbUser,static::$dbPassword);
        static::$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }

    static public function getDb() {
        if(is_null(static::$db)) {
            static::setDb();
        }
        return static::$db;
    }
}