<?php

require_once 'traits/DbConnect.php';

abstract class Table
{
    use DbConnect;

    protected $attributes = [];
    protected $parametersString = 'DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';

    static public function getTableName() {
        $name = get_called_class();
        $name = strtolower($name);
        $name = str_replace('table','',$name);
        $name .= 's';
        return $name;
    }

    public function getAttributes(){
        return $this->attributes;
    }

    static public function getAttributesString($attributes) {
        $attrString = '';
        foreach ($attributes as $key => $val) {

            $attrString .= $key . ' ' . $val . ',' ;
        }
        $attrString = substr_replace($attrString,"",-1);
        return $attrString;
    }

    public function getParametersString() {
        return $this->parametersString;
    }

    public function createTable() {
        static::getDb()->exec('CREATE TABLE ' . static::getTableName() . '( ' . static::getAttributesString($this->getAttributes()) . ') ' . static::getParametersString());
    }
}